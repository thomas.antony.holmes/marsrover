from unittest import TestCase
from rover import Rover
from coordinates import Coordinates
from direction import Direction

class TestRover(TestCase):
    def setUp(self):
        super().setUp()
        self.world = Coordinates(x=5, y=5)

    def test_rover_position_default(self):
        """
        If we pass no parameters aside from the world to the rover, check that the rover
        positioning and direction is correct.
        """
        rover = Rover(self.world)
        self.assertEqual(rover._position.x, 0)
        self.assertEqual(rover._position.y, 0)
        self.assertEqual(rover._direction, Direction.NORTH)
        self.assertEqual(rover.lost, False)

    def test_rover_describe_default(self):
        """
        If we pass no parameters aside from the world to the rover, check that
        what printing the rover produces is correct
        """
        rover = Rover(self.world)
        self.assertEqual(str(rover), ('(0, 0, N)'))

    def test_rover_describe_lost(self):
        """
        If we pass parameters that would mean the rover is lost, check that
        what printing the rover produces is correct
        """
        rover = Rover(self.world, 1, 0, 'S')
        for _ in range(0, 10):
            rover.run_command('F')
        self.assertEqual(str(rover), ('(1, 0, S) LOST'))

    def test_rover_is_lost(self):
        """
        If we pass parameters that would mean the rover is lost, check that
        what printing the rover produces is correct
        """
        rover = Rover(self.world, 10, 00)
        self.assertTrue(rover.lost)

    def test_rover_last_known(self):
        """
        Check that the last known position returns correctly
        """
        rover = Rover(self.world)
        for _ in range(0, 10):
            rover.run_command('F')

        self.assertEqual(rover.last_known_position.x, 0)
        self.assertEqual(rover.last_known_position.y, 5)
        self.assertTrue(rover.lost)
        rover = Rover(self.world)

        for _ in range(0, 4):
            rover.run_command('F')
        
        rover.run_command('L')
        for _ in range(0, 3):
            rover.run_command('F')
                    
        self.assertEqual(rover.last_known_position.x, 0)
        self.assertEqual(rover.last_known_position.y, 4)
        self.assertTrue(rover.lost)

    def test_rover_move_forward(self):
        """
        Check that the rover changes position when provided the 'F' command.
        """
        rover = Rover(self.world)
        rover.run_command('F')
        self.assertEqual(rover._position.y, 1)
        self.assertEqual(rover._direction, Direction.NORTH)

        rover.run_command('F')
        self.assertEqual(rover._position.y, 2)
        self.assertEqual(rover._direction, Direction.NORTH)

    def test_rover_rotate_left(self):
        """
        Check that the rover changes direction when provided the 'L' command.
        """
        rover = Rover(self.world)
        rover.run_command('L')
        self.assertEqual(rover._position.x, 0)
        self.assertEqual(rover._direction, Direction.WEST)

        rover.run_command('L')
        self.assertEqual(rover._position.x, 0)
        self.assertEqual(rover._direction, Direction.SOUTH)

        rover.run_command('L')
        self.assertEqual(rover._position.x, 0)
        self.assertEqual(rover._direction, Direction.EAST)

        rover.run_command('L')
        self.assertEqual(rover._position.x, 0)
        self.assertEqual(rover._direction, Direction.NORTH)

    def test_rover_rotate_right(self):
        """
        Check that the rover changes direction when provided the 'R' command.
        """
        rover = Rover(self.world)
        rover.run_command('R')
        self.assertEqual(rover._position.x, 0)
        self.assertEqual(rover._direction, Direction.EAST)

        rover.run_command('R')
        self.assertEqual(rover._position.x, 0)
        self.assertEqual(rover._direction, Direction.SOUTH)

        rover.run_command('R')
        self.assertEqual(rover._position.x, 0)
        self.assertEqual(rover._position.x, 0)
        self.assertEqual(rover._direction, Direction.WEST)

        rover.run_command('R')
        self.assertEqual(rover._position.x, 0)
        self.assertEqual(rover._position.x, 0)
        self.assertEqual(rover._direction, Direction.NORTH)

