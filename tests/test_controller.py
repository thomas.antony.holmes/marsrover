from unittest import TestCase
from controller import Controller
from coordinates import Coordinates

class TestRoverController(TestCase):
    def setUp(self):
        super().setUp()

    def test_parse_world_input(self):
        """Check controller accepts a valid world input
        and correctly sets the world.
        """
        controller = Controller()
        res = controller._parse_world_input('4 5')
        self.assertTrue(res)
        self.assertEqual(controller.world.x, 4)
        self.assertEqual(controller.world.y, 5)

    def test_parse_world_input_bad_input(self):
        """Check controller won't accept a bad input such
        as letters instead of numbers
        """
        controller = Controller()
        res = controller._parse_world_input('A 3')
        self.assertFalse(res)
        self.assertIsNone(controller.world)

    def test_parse_rover_input(self):
        """Check controller will accept a basic rover input
        """
        controller = Controller()
        controller.world = Coordinates(10, 10)
        res = controller._parse_rover_input('(2, 3, N) FLLFR')
        self.assertTrue(res)
        self.assertEqual(len(controller.rovers), 1)
    
    def test_parse_rover_input_bad_input(self):
        """Check controller won't accept a bad input for
        the rover
        """
        controller = Controller()
        controller.world = Coordinates(10, 10)
        # Try bad location
        res = controller._parse_rover_input('(2) FLLFR')
        self.assertFalse(res)

    def test_parse_rover_input_bad_input_command(self):
        """Check controller won't accept a bad command input for
        the rover
        """
        controller = Controller()
        controller.world = Coordinates(10, 10)
        # Try invalid direction
        res = controller._parse_rover_input('(2, 3) SADASD')
        self.assertFalse(res)
    