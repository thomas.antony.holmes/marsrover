class Coordinates():
    """
    Object to represent co-ordinates within the world
    """

    # Sidenote, could simply use a namedtuple instead of a class
    # but this offers better flexibility if we needed to add more
    # methods later on.

    def __init__(self, x=0, y=0):
        try:
            self.x = int(x)
        except TypeError:
            raise TypeError('Invalid x co-ordinate')

        try:
            self.y = int(y)
        except TypeError:
            raise TypeError('Invalid y co-ordinate')