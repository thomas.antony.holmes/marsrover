from coordinates import Coordinates
from direction import Direction

class Rover():
    """Rover object to represent a Mars Rover

    Attributes:
        lost (bool) True if the rover is currently lost.
        last_known_position (coordinate.Coordinate) The last known position of the rover.
        last_known_direction (direction.Direction) The current direction of the rover.
    """
    def __init__(self, world, x=0, y=0, direction=None):
        self._world = world
        self._set_position(Coordinates(x, y))
        self._set_direction(direction)

    @property
    def lost(self):
        # Check x-axis out of bounds
        if self._position.x < 0 or self._position.x > self._world.x:
            return True
        
        # Check y-axis out of bounds
        if self._position.y < 0 or self._position.y > self._world.y:
            return True

        return False

    def _update_last_known_position(self):
        # Don't update if already lost
        if not self.lost:
            self.last_known_position = self._position

    def _update_last_known_direction(self):
        # Don't update if already lost
        if not self.lost:
            self.last_known_direction = self._direction

    def _rotate_left(self):
        new_direction_index = (self._direction.value - 1) % len(list(Direction))
        self._direction = Direction(new_direction_index)

    def _rotate_right(self):
        new_direction_index = (self._direction.value + 1) % len(list(Direction))
        self._direction = Direction(new_direction_index)

    def _move_forward(self):
        curr_position = self._position
        if self._direction == Direction.NORTH:
            self._set_position(Coordinates(curr_position.x, curr_position.y + 1))
        elif self._direction == Direction.EAST:
            self._set_position(Coordinates(curr_position.x + 1, curr_position.y))
        elif self._direction == Direction.SOUTH:
            self._set_position(Coordinates(curr_position.x, curr_position.y - 1))
        elif self._direction == Direction.WEST:
            self._set_position(Coordinates(curr_position.x - 1, curr_position.y))

    def _set_position(self, new_position):
        # Check position has been provided
        if not new_position:
            # Set default
            new_position = Coordinates(0,0)
        
        # Check type of new position
        if not isinstance(new_position, Coordinates):
            raise TypeError('new_position must be instance of coordinates.Coordinates')

        self._position = new_position
        self._update_last_known_position()

    def run_command(self, command):
        if not isinstance(command, str):
            raise TypeError('Commands must be instance of string')
        if not len(command) == 1:
            raise ValueError('Commands must be 1 character in length')
    
        if command == 'F':
            self._move_forward()
        elif command == 'L':
            self._rotate_left()
        elif command == 'R':
            self._rotate_right()

    def _set_direction(self, new_direction):
        # Check direction has been provided
        if not new_direction:
            # Default to North
            new_direction = Direction.NORTH
        else:
            if new_direction == 'N':
                new_direction = Direction.NORTH
            elif new_direction == 'E':
                new_direction = Direction.EAST
            elif new_direction == 'S':
                new_direction = Direction.SOUTH
            elif new_direction == 'W':
                new_direction = Direction.WEST
            else:
                return False

        self._direction = new_direction
        self._update_last_known_direction()
        return True

    def __str__(self):
        """Returns string representation for when Rover object is cast to string

        Returns:
            str: A string representation in format of '(0, 0, N) LOST'
        """
        lost_msg = 'LOST' if self.lost else ''
        return '({}, {}, {}) {}'.format(self.last_known_position.x, self.last_known_position.y, self._direction, lost_msg).strip()
