# MarsRover
This project contains a program to take commands and move one or more robots around Mars.

## Usage
The program is written in Python 3.9, and should be ran using the following command from the project root.

`$ python3 main.py`

## Tests
Tests can either be ran using Python's default unittest module, or alternatively a runner such as nose 2 can be used:

`$ pip3 install nose2`
`$ nose2`
