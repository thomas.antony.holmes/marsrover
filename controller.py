import re
from rover import Rover
from coordinates import Coordinates

class Controller():
    rovers = [] # List of tuples (Rover, command)
    world = None

    def run(self):
        # Get info about world from user
        while self.world is None:
            self._parse_world_input(input('World: '))

        # Get rover commands from user
        while True:
            command_input = input('Enter rover info or press enter to execute commands:')
            if command_input == '':
                # User enter empty input, execute commands
                if self._execute_rover_commands():
                    # If successful, exit
                    break
            else:
                self._parse_rover_input(command_input)

    def _execute_rover_commands(self):
        if not len(self.rovers):
            print('No rovers have been entered.')
        else:
            for rover, command in self.rovers:
                # Iterate each command
                for char in list(command):
                    rover.run_command(char)
                # Output new status to user
                print(rover)

    def _parse_rover_input(self, value=''):
        pattern = r'^\((\d*)\, (\d*), ([NESW]{1})\) ([FLR].*)$'
        try:
            match = re.findall(pattern, value)[0]
            self.rovers.append((Rover(self.world, match[0], match[1], match[2]), match[3]))
            return True
        except:
            print('Invalid command input. Try again.')
            return False

    def _parse_world_input(self, value=''):
        pattern = r'^(\d*) (\d*)$'
        world = re.search(pattern, value)
        try:
            self.world = Coordinates(world.group(1), world.group(2))
            return True
        except Exception:
            return False

